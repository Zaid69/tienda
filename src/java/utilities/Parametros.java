/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose sql | Templates
 * and open the template in the editor.
 */
package utilities;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import javax.servlet.http.HttpServletRequest;


/**
 *
 * @author tlacaelel21
 */
public class Parametros {
    public String ReadParameter(String strParameter, HttpServletRequest request) {
        Properties prop = new Properties();
        InputStream input = null;
        String strResult = "";
        try {
            input = getClass().getResourceAsStream("configweb_.properties");
            // load a properties file
            prop.load(input);
            // get the property value and print it out
            strResult = prop.getProperty(strParameter);
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return strResult;
    }//ReadParameter
}

